FROM alpine

WORKDIR /work

COPY file.txt file.txt
RUN echo '' >> file.txt

CMD ["cat", "file.txt"]
